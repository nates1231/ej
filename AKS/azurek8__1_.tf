# Configure the Microsoft Azure Provider2
provider "azurerm" {
    # The "feature" block is required for AzureRM provider 2.x. 
    # If you're using version 1.x, the "features" block is not allowed.
    version = "~>2.0"
    features {}
}

variable "aks_client_id" {
  description = "GUID of the AKS Service Principal"
}

variable "aks_client_secret" {
  description = "secret of the AKS Service Principal"
}

variable "myTag" {
  description = "My Default Tag"
  default = "terraform-test"
}

variable "mylocation" {
  description = "My Default Tag"
  default = "eastus"
}

variable "myresourceGroup" {
  description = "My Resource Group Name"
  default = "example-resources"
}

variable "myFlavor" {
  description = "My K8 Nodes Flavors"
  default = "Standard_D2_v2"
}

variable "myNodeCount" {
  description = "Number of K8 Nodes "
  default = 1
}

variable "configPort" {
  description = "SSH Enabled Port "
  default = "22"
}

resource "azurerm_resource_group" "example" {
  name     = var.myresourceGroup
  location = var.mylocation
}

resource "azurerm_private_dns_zone" "example" {
  name                = "mydomain.com"
  resource_group_name = azurerm_resource_group.example.name
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "myNetworkSecurityGroup"
    location            = azurerm_resource_group.example.location
    resource_group_name = azurerm_resource_group.example.name
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = var.configPort
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = var.myTag
    }
}

resource "azurerm_kubernetes_cluster" "example" {
  name                = "example-aks1"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  dns_prefix          = "exampleaks1"
#  private_dns_zone_id = azurerm_private_dns_zone.example.id

  default_node_pool {
    name       = "default"
    node_count = var.myNodeCount
    vm_size    = var.myFlavor
  }

  #service_principal {
    #client_id     = var.aks_client_id
    #client_secret = var.aks_client_secret
  #}

 identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Production"
  }
}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.example.kube_config.0.client_certificate
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.example.kube_config_raw
}

output "private_dns_zone_id" {
  value = azurerm_private_dns_zone.example.id
}
